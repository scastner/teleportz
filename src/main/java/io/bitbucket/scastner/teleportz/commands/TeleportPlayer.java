package io.bitbucket.scastner.teleportz.commands;

import io.bitbucket.scastner.teleportz.Config;
import io.bitbucket.scastner.teleportz.TeleportCommand;
import io.bitbucket.scastner.teleportz.TeleportZ;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TeleportPlayer extends TeleportCommand implements CommandExecutor {
    public TeleportPlayer(TeleportZ plugin) {
        super(plugin);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;

            if (args.length == 1) {
                teleportToPlayerExact(player, args[0]);
            } else if (args.length == 3) {
                teleportToCoordinates(player, args);
            } else {
                sendMessage(player, command.getUsage());
            }
        } else if (sender != null) {
            sendMessage(sender, String.format("%sThis command is only usable by players", ChatColor.RED));
        }

        return true;
    }

    private void teleportToPlayerExact(Player player, String name) {
        Player target = player.getServer().getPlayerExact(name);
        if (target != null) {
            if (player.getUniqueId().compareTo(target.getUniqueId()) != 0) {
                teleportToPlayer(player, target, false);
            } else {
                sendMessage(player, formatConfigMessage(Config.cannotTeleportToSelf, player.getDisplayName(), name));
            }
        } else {
            sendMessage(player, formatConfigMessage(Config.playerNotFound, player.getDisplayName(), name));
        }
    }

    private void teleportToCoordinates(Player player, String[] args) {
        try {
            float x = Float.parseFloat(args[0]);
            float y = Float.parseFloat(args[1]);
            float z = Float.parseFloat(args[2]);

            teleportToXYZ(player, x, y, z);
        } catch (Exception e) {
            sendMessage(player, Config.invalidCoordinates);
        }
    }
}
