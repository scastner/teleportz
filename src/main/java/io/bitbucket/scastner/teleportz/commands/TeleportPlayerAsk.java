package io.bitbucket.scastner.teleportz.commands;

import io.bitbucket.scastner.teleportz.Config;
import io.bitbucket.scastner.teleportz.TeleportCommand;
import io.bitbucket.scastner.teleportz.TeleportRequest;
import io.bitbucket.scastner.teleportz.TeleportZ;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TeleportPlayerAsk extends TeleportCommand implements CommandExecutor {
    public TeleportPlayerAsk(TeleportZ plugin) {
        super(plugin);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;

            if (args.length == 1) {
                Player target = player.getServer().getPlayerExact(args[0]);
                if (target != null) {
                    if (!player.equals(target)) {
                        TeleportRequest.add(target, player, TeleportRequest.Type.TPA);

                        sendMessage(target, formatConfigMessage(Config.teleportToPlayerRequest, player.getDisplayName(), args[0]));
                        sendMessage(target, teleportAskMessage(player));
                    } else {
                        sendMessage(player, formatConfigMessage(Config.cannotTeleportToSelf, player.getDisplayName(), args[0]));
                    }

                } else {
                    sendMessage(player, formatConfigMessage(Config.playerNotFound, player.getDisplayName(), args[0]));
                }
            } else {
                sendMessage(player, command.getUsage());
            }
        } else if (sender != null) {
            sendMessage(sender, String.format("%sThis command is only usable by players", ChatColor.RED));
        }

        return true;
    }
}
