package io.bitbucket.scastner.teleportz.commands;

import io.bitbucket.scastner.teleportz.Config;
import io.bitbucket.scastner.teleportz.Database;
import io.bitbucket.scastner.teleportz.TeleportCommand;
import io.bitbucket.scastner.teleportz.TeleportZ;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TeleportBack extends TeleportCommand implements CommandExecutor {
    public TeleportBack(TeleportZ plugin) {
        super(plugin);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;

            Database.getLastLocation(player, location -> {
                if (location != null) {
                    teleportToLocation(player, location);
                } else {
                    sendMessage(sender, Config.lastLocationNotFound);
                }
            });
        } else if (sender != null) {
            sendMessage(sender, String.format("%sThis command is only usable by players", ChatColor.RED));
        }

        return true;
    }
}
