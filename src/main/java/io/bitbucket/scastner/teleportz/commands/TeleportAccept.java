package io.bitbucket.scastner.teleportz.commands;

import io.bitbucket.scastner.teleportz.Config;
import io.bitbucket.scastner.teleportz.TeleportCommand;
import io.bitbucket.scastner.teleportz.TeleportRequest;
import io.bitbucket.scastner.teleportz.TeleportZ;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.UUID;

public class TeleportAccept extends TeleportCommand implements CommandExecutor {
    public TeleportAccept(TeleportZ plugin) {
        super(plugin);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;

            if (args.length == 0) {
                List<String[]> available = TeleportRequest.getRequests(player);
                if (available.size() > 1) {
                    sendMessage(player, Config.multipleTeleportRequests);
                    sendMessage(player, command.getUsage());
                    available.forEach(request -> {
                        Player target = sender.getServer().getPlayer(UUID.fromString(request[0]));
                        if (request[1].equals(TeleportRequest.Type.TPA.name())) {
                            sendMessage(player, "  " + request[1] + " -> " + target.getDisplayName());
                        } else if (request[1].equals(TeleportRequest.Type.TPAHERE.name())) {
                            sendMessage(player, "  " + request[1] + " <- " + target.getDisplayName());
                        }
                    });
                } else if (available.size() == 1) {
                    args = new String[]{available.get(0)[0]};
                }
            }

            if (args.length == 1) {
                Player target = player.getServer().getPlayerExact(args[0]);
                if (target != null) {
                    TeleportRequest.Type type = TeleportRequest.findAndRemove(player, target);

                    if (type != null) {
                        if (type == TeleportRequest.Type.TPA) {
                            teleportToPlayer(target, player);
                        } else if (type == TeleportRequest.Type.TPAHERE) {
                            teleportToPlayer(player, target);
                        }
                    } else {
                        sendMessage(player, formatConfigMessage(Config.noTeleportRequestFound, player.getDisplayName(), args[0]));
                    }
                } else {
                    sendMessage(player, formatConfigMessage(Config.playerNotFound, player.getDisplayName(), args[0]));
                }
            } else {
                sendMessage(player, command.getUsage());
            }
        } else if (sender != null) {
            sendMessage(sender, String.format("%sThis command is only usable by players", ChatColor.RED));
        }

        return true;
    }
}
