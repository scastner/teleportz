package io.bitbucket.scastner.teleportz;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.TextComponent;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.economy.EconomyResponse;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent;

public class TeleportCommand {
    private TeleportZ plugin;

    public TeleportCommand(TeleportZ plugin) {
        this.plugin = plugin;
    }

    protected String formatConfigMessage(String msg, String player, String target) {
        if (player != null) {
            msg = msg.replace("{player}", player);
        }

        if (target != null) {
            msg = msg.replace("{target}", target);
        }

        return msg;
    }

    protected void sendMessage(CommandSender sender, String msg) {
        sender.sendMessage(Config.chatPrefix + " " + msg);
    }

    protected void sendMessage(CommandSender sender, BaseComponent[] components) {
        BaseComponent[] msg = new ComponentBuilder(Config.chatPrefix + " ")
            .append(components)
            .create();
        sender.spigot().sendMessage(msg);
    }

    protected void teleportToPlayer(Player player, Player target) {
        teleportToPlayer(player, target, true);
    }

    protected void teleportToPlayer(Player player, Player target, boolean sendMessages) {
        teleportToLocation(player, target.getLocation());

        if (sendMessages) {
            sendMessage(player, formatConfigMessage(Config.youTeleportedToPlayer, player.getDisplayName(), target.getDisplayName()));
            sendMessage(target, formatConfigMessage(Config.playerTeleportedToYou, player.getDisplayName(), target.getDisplayName()));
        }

        plugin.getLogger().info(player.getDisplayName() + " teleported to " + target.getDisplayName());
    }

    protected void teleportToXYZ(Player player, float x, float y, float z) {
        teleportToLocation(player, new Location(player.getWorld(), x, y, z));
    }

    protected void teleportToLocation(Player player, Location location) {
        Economy economy = plugin.getEconomy();
        if (economy != null && Config.economyEnabled) {
            if (!player.hasPermission("teleportz.free")) {
                double cost = Config.defaultCost;
                if (player.hasPermission("teleportz.vip")) {
                    cost = Config.vipCost;
                }

                EconomyResponse response = economy.withdrawPlayer(player, cost);
                if (response.transactionSuccess()) {
                    sendMessage(player, Config.economyWithdrawSuccess
                        .replace("{cost}", String.valueOf(cost))
                        .replace("{balance}", String.valueOf(economy.getBalance(player)))
                    );
                } else {
                    sendMessage(player, Config.economyWithdrawFailed
                        .replace("{cost}", String.valueOf(cost))
                        .replace("{balance}", String.valueOf(economy.getBalance(player)))
                    );
                    return;
                }
            }
        }

        player.teleport(location, PlayerTeleportEvent.TeleportCause.PLUGIN);
        plugin.getLogger().info(player.getDisplayName() + " teleported to " + location.toString());
    }

    protected BaseComponent[] teleportAskMessage(Player player) {
        TextComponent tpaccept = new TextComponent("ACCEPT");
        tpaccept.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/tpaccept " + player.getName()));

        TextComponent tpdeny = new TextComponent("DENY");
        tpdeny.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/tpdeny " + player.getName()));

        return new ComponentBuilder("")
            .append("[")
            .color(ChatColor.WHITE)
            .append(tpaccept)
            .color(ChatColor.GREEN)
            .append("] [")
            .color(ChatColor.WHITE)
            .append(tpdeny)
            .color(ChatColor.RED)
            .append("]")
            .color(ChatColor.WHITE)
            .create();
    }
}
