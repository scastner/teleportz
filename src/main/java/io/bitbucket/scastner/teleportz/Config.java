package io.bitbucket.scastner.teleportz;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.plugin.Plugin;

public class Config {
    // TeleportZ
    public static String chatPrefix;
    // Economy
    public static boolean economyEnabled;
    public static double defaultCost;
    public static double vipCost;
    // Requests
    public static long requestTimeout;
    // Death
    public static boolean saveDeathLocation;
    // Messages
    public static String playerTeleportedToYou;
    public static String youTeleportedToPlayer;
    public static String multipleTeleportRequests;
    public static String playerNotFound;
    public static String noTeleportRequestFound;
    public static String teleportToPlayerRequest;
    public static String teleportPlayerHereRequest;
    public static String cannotTeleportToSelf;
    public static String lastLocationNotFound;
    public static String invalidCoordinates;
    public static String economyWithdrawSuccess;
    public static String economyWithdrawFailed;

    public static void init(Plugin plugin) {
        plugin.getConfig().options().copyDefaults(true);
        plugin.saveConfig();
        // TeleportZ
        chatPrefix = translateString(plugin, "TeleportZ.Prefix");
        // Economy
        economyEnabled = plugin.getConfig().getBoolean("Economy.Enabled");
        defaultCost = plugin.getConfig().getDouble("Economy.Cost.Default");
        vipCost = plugin.getConfig().getDouble("Economy.Cost.VIP");
        // Requests
        requestTimeout = plugin.getConfig().getLong("Request.Timeout");
        // Death
        saveDeathLocation = plugin.getConfig().getBoolean("Death.Enabled");
        // Messages
        playerNotFound = translateString(plugin, "Messages.PlayerNotFound");
        playerTeleportedToYou = translateString(plugin, "Messages.PlayerTeleportedToYou");
        youTeleportedToPlayer = translateString(plugin, "Messages.YouTeleportedToPlayer");
        multipleTeleportRequests = translateString(plugin, "Messages.MultipleTeleportRequests");
        noTeleportRequestFound = translateString(plugin, "Messages.NoTeleportRequestFound");
        teleportToPlayerRequest = translateString(plugin, "Messages.TeleportToPlayerRequest");
        teleportPlayerHereRequest = translateString(plugin, "Messages.TeleportPlayerHereRequest");
        cannotTeleportToSelf = translateString(plugin, "Messages.CannotTeleportToSelf");
        lastLocationNotFound = translateString(plugin, "Messages.LastLocationNotFound");
        invalidCoordinates = translateString(plugin, "Messages.InvalidCoordinates");
        economyWithdrawSuccess = translateString(plugin, "Messages.EconomyWithdrawSuccess");
        economyWithdrawFailed = translateString(plugin, "Messages.EconomyWithdrawFailed");
    }

    private static String translateString(Plugin plugin, String path) {
        return ChatColor.translateAlternateColorCodes('&', "&r" + plugin.getConfig().getString(path));
    }
}
