package io.bitbucket.scastner.teleportz;

import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TeleportRequest {
    public enum Type {
        TPA,
        TPAHERE
    }

    private static HashMap<String, HashMap<String, Type>> requests = new HashMap<>();
    private static long timeout;
    private static Plugin plugin;

    static void init(Plugin plugin, long timeout) {
        TeleportRequest.plugin = plugin;
        TeleportRequest.timeout = timeout * 20;
    }

    public static void add(Player toPlayer, Player fromPlayer, Type type) {
        if (!requests.containsKey(toPlayer.getUniqueId().toString())) {
            requests.put(toPlayer.getUniqueId().toString(), new HashMap<>());
        }
        requests.get(toPlayer.getUniqueId().toString()).put(fromPlayer.getUniqueId().toString(), type);

        BukkitRunnable r = new BukkitRunnable() {
            @Override
            public void run() {
                if (requests.get(toPlayer.getUniqueId().toString()).containsKey(fromPlayer.getUniqueId().toString())) {
                    requests.get(toPlayer.getUniqueId().toString()).remove(fromPlayer.getUniqueId().toString());
                    plugin.getServer().getLogger().info(Config.chatPrefix + " Teleport request has expired");
                }
            }
        };
        r.runTaskLaterAsynchronously(plugin, timeout);
    }

    public static List<String[]> getRequests(Player player) {
        List<String[]> available = new ArrayList<>();
        if (requests.containsKey(player.getUniqueId().toString())) {
            requests.get(player.getUniqueId().toString()).forEach((key, value) -> available.add(new String[]{key, value.name()}));
        }
        return available;
    }

    public static Type findAndRemove(Player player, Player target) {
        if (requests.containsKey(player.getUniqueId().toString())) {
            return requests.get(player.getUniqueId().toString()).remove(target.getUniqueId().toString());
        }
        return null;
    }
}
