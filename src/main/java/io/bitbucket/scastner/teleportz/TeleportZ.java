package io.bitbucket.scastner.teleportz;

import io.bitbucket.scastner.teleportz.commands.*;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

public class TeleportZ extends JavaPlugin {
    private Economy economy = null;

    @Override
    public void onEnable() {
        Config.init(this);
        Database.init(this);
        TeleportRequest.init(this, Config.requestTimeout);

        getCommand("tp").setExecutor(new TeleportPlayer(this));
        getCommand("tphere").setExecutor(new TeleportPlayerHere(this));
        getCommand("tpa").setExecutor(new TeleportPlayerAsk(this));
        getCommand("tpahere").setExecutor(new TeleportPlayerHereAsk(this));
        getCommand("tpaccept").setExecutor(new TeleportAccept(this));
        getCommand("tpdeny").setExecutor(new TeleportDeny(this));
        getCommand("back").setExecutor(new TeleportBack(this));

        getServer().getPluginManager().registerEvents(new TeleportListener(), this);

        if (Bukkit.getPluginManager().getPlugin("Vault") != null) {
            RegisteredServiceProvider<Economy> provider = getServer().getServicesManager().getRegistration(Economy.class);
            if (provider != null) {
                economy = provider.getProvider();
            }
        }
    }

    @Override
    public void onDisable() {
        Database.close();
    }

    public Economy getEconomy() {
        return economy;
    }
}
