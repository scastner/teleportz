package io.bitbucket.scastner.teleportz;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.File;
import java.sql.*;
import java.util.UUID;

public class Database {
    public interface Callback<T> {
        void execute(T result);
    }

    private static JavaPlugin plugin;
    private static Connection connection;

    private static final String sqlLastLocationTable = "" +
        "CREATE TABLE IF NOT EXISTS last_location (" +
        "   player_id TEXT PRIMARY KEY," +
        "   world TEXT NOT NULL," +
        "   x REAL NOT NULL," +
        "   y REAL NOT NULL," +
        "   z REAL NOT NULL" +
        ") WITHOUT ROWID";
    private static final String sqlSaveLastLocation = "" +
        "INSERT OR REPLACE INTO last_location(player_id, world, x, y, z) " +
        "VALUES(?, ?, ?, ?, ?)";
    private static final String sqlGetLastLocation = "" +
        "SELECT world, x, y, z FROM last_location WHERE player_id = ?";

    public static void init(JavaPlugin plugin) {
        try {
            Database.plugin = plugin;
            if (!plugin.getDataFolder().exists()) {
                if (plugin.getDataFolder().mkdir()) {
                    new File(plugin.getDataFolder(), "teleportz.db");
                }
            }
            connection = DriverManager.getConnection("jdbc:sqlite:" + plugin.getDataFolder().getAbsolutePath() + "/teleportz.db");
            createLastLocationTable();
        } catch (SQLException e) {
            logException("Open database", e);
        }
    }

    public static void close() {
        try {
            connection.close();
        } catch (SQLException e) {
            logException("Close database", e);
        }
    }

    public static void saveLastLocation(Player player) {
        saveLastLocation(player, player.getLocation());
    }

    public static void saveLastLocation(Player player, Location location) {
        if (connection != null) {
            BukkitRunnable r = new BukkitRunnable() {
                @Override
                public void run() {
                    try (PreparedStatement stmt = connection.prepareStatement(sqlSaveLastLocation)) {
                        stmt.setString(1, player.getUniqueId().toString());
                        stmt.setString(2, location.getWorld().getUID().toString());
                        stmt.setDouble(3, location.getX());
                        stmt.setDouble(4, location.getY());
                        stmt.setDouble(5, location.getZ());
                        stmt.executeUpdate();
                    } catch (SQLException e) {
                        logException("saveLastLocation", e);
                    }
                }
            };
            r.runTaskAsynchronously(plugin);
        }
    }

    public static void getLastLocation(Player player, Callback<Location> callback) {
        if (connection != null) {
            BukkitRunnable r = new BukkitRunnable() {
                @Override
                public void run() {
                    try (PreparedStatement stmt = connection.prepareStatement(sqlGetLastLocation)) {
                        stmt.setString(1, player.getUniqueId().toString());
                        ResultSet rs = stmt.executeQuery();
                        if (rs.next()) {
                            callback.execute(new Location(
                                player.getServer().getWorld(UUID.fromString(rs.getString(1))),
                                rs.getDouble(2),
                                rs.getDouble(3),
                                rs.getDouble(4)
                            ));
                            return;
                        }
                    } catch (SQLException e) {
                        logException("getLastLocation", e);
                    }
                    callback.execute(null);
                }
            };
            r.runTaskAsynchronously(plugin);
        } else {
            callback.execute(null);
        }
    }

    private static void createLastLocationTable() {
        if (connection != null) {
            try (Statement stmt = connection.createStatement()) {
                stmt.executeUpdate(sqlLastLocationTable);
            } catch (SQLException e) {
                logException("createLastLocationTable", e);
            }
        }
    }

    private static void logException(String message, Exception e) {
        plugin.getLogger().severe(message + ":\n" + String.format("%s" + e.getMessage(), ChatColor.RED));
    }
}
